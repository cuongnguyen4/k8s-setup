# sudo apt-mark hold kubelet kubeadm kubectl

# sudo swapoff -a

# sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# sudo modprobe overlay
# sudo modprobe br_netfilter

sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

sudo sysctl --system

sudo tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF

mkdir -p /etc/containerd
containerd config default>/etc/containerd/config.toml

sudo systemctl restart containerd
sudo systemctl enable containerd
systemctl status containerd


# lsmod | grep br_netfilter
# sudo systemctl enable kubelet


# cd docker-ce
# sudo dpkg -i *

# cd k8s-image
# for x in *.tar; do docker load < $x && echo "Loaded docker $x"; done;

# for x in *.tar; do ctr -n k8s.io images import $x && echo "Loaded docker $x"; done;

# sudo kubeadm init \
#   --pod-network-cidr=10.244.0.0/16 \
#   --cri-socket unix:///run/containerd/containerd.sock \
#   --apiserver-advertise-address=192.168.50.10

# cd nfs
# sudo dpkg -i *



```sh
sudo nano /etc/containerd/config.toml
```

```sh
# Config file is parsed as version 1 by default.
# To use the long form of plugin names set "version = 2"
[plugins.cri.registry.mirrors]
  [plugins.cri.registry.mirrors."10.10.20.34"]
    endpoint = ["http://10.10.20.34"]
```

```sh
sudo systemctl restart containerd
```

helm repo add nfs-ganesha-server-and-external-provisioner https://kubernetes-sigs.github.io/nfs-ganesha-server-and-external-provisioner/
helm install nfs-provisioner nfs-ganesha-server-and-external-provisioner/nfs-server-provisioner


helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace



sudo kubeadm reset
rm -rf .kube/
sudo rm -rf /etc/kubernetes/
sudo rm -rf /var/lib/kubelet/
sudo rm -rf /var/lib/etcd

unzip file.zip -d destination_folder

