hostnamectl set-hostname aiontech-k8s-worker40
```sh
kubeadm init --control-plane-endpoint="10.10.20.33:6443" --upload-certs --apiserver-advertise-address=10.10.20.30 --pod-network-cidr=10.244.0.0/16
```


```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```


```
kubeadm token create --print-join-command
```


kubeadm join 10.10.20.33:6443 --token g67fq7.qp3gjmlkrla05po6 \
        --discovery-token-ca-cert-hash sha256:9d2e3c7249f5ae3f065d4debd7e5e215ccd68bf44f1298da4c1dbc0cfd7ce126 \
        --control-plane --certificate-key 3310d7a82491f1bc5a82e6848e4e4c68ae8e4ae71b7c6701af51f57981d0fd5e
