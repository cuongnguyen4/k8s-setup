#!/bin/bash

mkdir ./images

# Read image list from the text file
while IFS= read -r image; do
    echo "============== $image =============="
    # Save the image to a tarball
    # Replace /local/path with the desired local directory
    image_name=$(echo $image | tr '/' '_' | tr ':' '_')
    image_file=./images/${image_name}.tar

    # Check if the tar file exists
    if [ -f "$image_file" ]; then
        echo "The tar file '$image_file' exists."
    else
        docker rmi $image
         # Pull the image
        docker pull $image
        docker save -o $image_file $image
        echo "$image saved as ./images/${image_name}.tar"
    fi
   
done < ./images.txt

echo "All images pulled and saved!"