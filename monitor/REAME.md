
### Add prometheus helm repo
```sh
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```

### Download locally
```sh
helm pull prometheus-community/kube-prometheus-stack --untar
```


### Create k8s namespace
```sh
kubectl create ns monitor
```


```sh
helm upgrade --install -n  monitor qkesmonitor prometheus-community/kube-prometheus-stack -f values.yaml
```

### Template
```sh
helm template -n  monitor qkesmonitor prometheus-community/kube-prometheus-stack -f values.yaml > sample.yaml
```

### Get image list
```sh
cat sample.yaml | grep "image:" | awk '{print $2}' | sed 's/"//g' > images.txt
```