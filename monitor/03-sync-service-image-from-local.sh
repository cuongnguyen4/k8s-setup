 #!/bin/bash

SERVICE_ISPC_FILE=ispc.txt

# Read image.txt line by line
while IFS='=' read -r source_image dst_image
do
    # Print the source and destination images
    echo "Source Image: $source_image, Destination Image: $dst_image"
    docker tag $source_image $dst_image
    docker push $dst_image
    docker rmi $dst_image
done < $SERVICE_ISPC_FILE
