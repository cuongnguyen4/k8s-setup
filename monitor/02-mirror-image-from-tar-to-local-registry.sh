#!/bin/bash

# Define local registry address
LOCAL_REGISTRY="reg.aiontech.vn"
SERVICE_ISPC_FILE=ispc.txt

rm -rf $SERVICE_ISPC_FILE

# Read image list from the text file
while IFS= read -r image; do
    echo "======== $image ======="
    img_file=$(echo $image | tr '/' '_' | tr ':' '_')
    echo "Loading  ./images/${img_file}.tar..."
    docker load -i ./images/${img_file}.tar
    local_image="$LOCAL_REGISTRY/$(echo "$image" | sed 's|^/*[^/]*/||')"
    echo $image=$local_image >> $SERVICE_ISPC_FILE

    # echo "Pushing $local_image to local registry..."
    # docker tag $image $local_image
    # docker push $local_image
done < images.txt

echo "All images loaded and pushed to the local registry!"