atEShCAAVzbXfaug


P: Pm4Aa1PujMlIAuTsKJtS8A==
UAUTH: Px0W3+nhw3htNZzfy8Kk4w==
UAGENT: S+zRQ+brrnQFaVagyOZt0Q==


R: 4SvW7lI6QKtrSdJ8
RP: O3ern6VfSPZuRxDn5qwuYw==


grant select on RAREP_AUTH.USER_ENTITY to RAR_AGENT;
SYNCER_URL: http://domain.store-qc.svc.cluster.local:4321


openssl genrsa -out ca.key 2048

openssl genpkey -algorithm RSA -out wildcard.key -pkeyopt rsa_keygen_bits:2048

openssl req -new -key wildcard.key -out wildcard.csr -subj "/CN=*.rarep.vn/O=BCA"

openssl x509 -req -days 365 -in wildcard.csr -signkey wildcard.key -out wildcard.crt

kubectl create secret tls wildcard-tls-secret --cert=wildcard.crt --key=wildcard.key

kubectl create secret tls <hello-app-tls> --cert=ca.crt --key=ca.private.key

tls:
  - hosts:
    - demo.mlopshub.com
    secretName: <hello-app-tls>

kubectl create secret generic rarep-keycloak-key --from-file=keycloak-public-key=jwt_keycloak.pub -n rarep


  volumes:
    - name: keycloak-key
      secret:
        secretName: rarep-keycloak-key
  volumeMounts:
    - name: keycloak-key
      mountPath: '/.key'
      readOnly: true