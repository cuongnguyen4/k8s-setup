#!/bin/bash

openssl enc -d -aes-256-cbc -pbkdf2 -in archive.tar.gz.enc | tar -xzvf -
