#!/bin/bash

tar -cvf - ./k8s ./bastion | gzip | openssl enc -aes-256-cbc -salt -pbkdf2 -out k8s-setup.tar.gz
