```sql
-- sqlplus sys/#Oracal123@//localhost:1521/ORCLCDB

alter session set "_ORACLE_SCRIPT"=true;

create user X identified by "#Oracal123";

-- GRANT ALL PRIVILEGES TO KEYCLOAK_DEV;

GRANT CREATE SESSION TO X;

GRANT CREATE TABLE TO X

GRANT UNLIMITED TABLESPACE TO X
```
