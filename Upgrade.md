https://itgix.com/blog/upgrading-a-kubernetes-cluster-by-using-kubeadm/

apt-mark unhold kubeadm

rm -rf /etc/apt/sources.list.d/kubernetes.list  
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.27/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.27/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
 apt-get update


sudo apt-get install -y kubeadm
sudo apt-mark hold kubeadm

kubeadm upgrade node

sudo apt-mark unhold kubelet kubectl

sudo apt-get update && sudo apt-get install -y kubelet kubectl

sudo apt-mark hold kubelet kubectl

sudo apt-mark hold kubelet kubectl\

sudo systemctl daemon-reload
sudo systemctl restart kubelet

## In case flannet failed
```
https://stackoverflow.com/questions/61373366/networkplugin-cni-failed-to-set-up-pod-xxxxx-network-failed-to-set-bridge-add

ip link set cni0 down && ip link set flannel.1 down 
ip link delete cni0 && ip link delete flannel.1
systemctl restart containerd && systemctl restart kubelet
```
