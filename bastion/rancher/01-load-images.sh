#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

cd $BASE/bastion/rancher/image

ls

for x in *.tar; do ctr -n k8s.io images import $x && echo "Loaded docker $x"; done;