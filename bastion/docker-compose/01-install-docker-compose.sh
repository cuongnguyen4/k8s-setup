#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

cp $BASE/bastion/docker-compose/package/docker-compose-linux-x86_64 /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose
cp /usr/local/bin/docker-compose /usr/local/bin/dc
