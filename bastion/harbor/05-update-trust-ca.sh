#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

cp $BASE/certs/certs.d/${LOCAL_REGISTRY}/${LOCAL_REGISTRY}.crt /etc/pki/ca-trust/source/anchors
update-ca-trust extract


curl https://$LOCAL_REGISTRY/v2/_catalog

curl -k http://$LOCAL_REGISTRY/v2/_catalog
