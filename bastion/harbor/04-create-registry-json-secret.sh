#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

REG_SECRET=`echo -n ${LOCAL_REGISTRY_USER}:${LOCAL_REGISTRY_PASSWORD} | base64 -w0`

echo '{ "auths": {}}' | jq ".auths += {\"${LOCAL_REGISTRY}\": {\"auth\": \"$REG_SECRET\",\"email\": \"ttdldc@gmail.com\"}}" | sed "s/REG_SECRET/$REG_SECRET/" | jq -c .> $LOCAL_SECRET_JSON


cat $LOCAL_SECRET_JSON