#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

sudo bash -c "cat <<EOF > /etc/docker/daemon.json
{
  \"insecure-registries\": [\"$LOCAL_REGISTRY\"]
}
EOF" && sudo systemctl restart docker