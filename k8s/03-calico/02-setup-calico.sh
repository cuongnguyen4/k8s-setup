#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

kubectl create -f $BASE/k8s/03-calico/tigera-operator.yaml

kubectl apply -f $BASE/k8s/03-calico/custom-resources.yaml