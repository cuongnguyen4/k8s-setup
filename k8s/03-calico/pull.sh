# docker pull quay.io/tigera/operator:v1.34.0

# docker save quay.io/tigera/operator:v1.34.0 > tigera_operator_v1.34.0.tar

# docker pull docker.io/calico/kube-controllers:v3.28.0
# docker pull docker.io/calico/typha:v3.28.0
# docker pull docker.io/calico/pod2daemon-flexvol:v3.28.0
# docker pull docker.io/calico/cni:v3.28.0
# docker pull docker.io/calico/node:v3.28.0

docker pull docker.io/calico/csi:v3.28.0
docker pull docker.io/calico/node-driver-registrar:v3.28.0
docker pull docker.io/calico/apiserver:v3.28.0

# docker save docker.io/calico/kube-controllers:v3.28.0 > calico_kube-controllers_v3.28.0.tar
# docker save docker.io/calico/typha:v3.28.0 > calico_typha_v3.28.0.tar

# docker save docker.io/calico/pod2daemon-flexvol:v3.28.0 > calico_pod2daemon-flexvol_v3.28.0.tar

# docker save docker.io/calico/cni:v3.28.0 > calico_cni_v3.28.0.tar
# docker save docker.io/calico/node:v3.28.0 > calico_node_v3.28.0.tar

docker save docker.io/calico/csi:v3.28.0 > calico_csi_v3.28.0.tar
docker save docker.io/calico/node-driver-registrar:v3.28.0 > calico_node-driver-registrar_v3.28.0.tar
docker save docker.io/calico/apiserver:v3.28.0 > calico_apiserver:v3.28.0.tar