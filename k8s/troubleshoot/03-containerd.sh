

/etc/containerd/config.toml
----
[plugins."io.containerd.grpc.v1.cri".registry]
   config_path = "/etc/containerd/certs.d"

/etc/containerd/certs.d/docker.io/hosts.toml
----
server = "https://registry-1.docker.io"
[host."https://{docker.mirror.url}"]
  capabilities = ["pull", "resolve"]

/etc/containerd/certs.d/{your.ip}:5000/hosts.toml
----
server = "https://registry-1.docker.io"
[host."http://{your.ip}:5000"]
  capabilities = ["pull", "resolve", "push"]
  skip_verify = true


mkdir -p /etc/containerd/certs.d/reg.c06.bca

vi /etc/containerd/certs.d/reg.c06.bca/hosts.toml

server = "https://registry-1.docker.io"
[host."http://reg.c06.bca"]
  capabilities = ["pull", "resolve", "push"]
  skip_verify = true

systemctl restart containerd

172.16.9.194 reg.c06.bca