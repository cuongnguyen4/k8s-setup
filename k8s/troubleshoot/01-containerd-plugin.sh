#!/bin/bash

# error execution phase preflight: [preflight] Some fatal errors occurred:
#         [ERROR CRI]: container runtime is not running: output: time="2024-12-24T10:56:40Z" level=fatal msg="validate service connection: 
# validate CRI v1 runtime API for endpoint \"unix:///var/run/containerd/containerd.sock\": rpc error: code = Unimplemented desc = unknown service runtime.v1.RuntimeService"
# , error: exit status 1

sudo cat > /etc/containerd/config.toml <<EOF
[plugins."io.containerd.grpc.v1.cri"]
  systemd_cgroup = true
EOF

sudo systemctl restart containerd