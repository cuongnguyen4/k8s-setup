#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

cd $BASE/k8s/05-redis

helm upgrade --install redis-standalone redis --namespace redis --create-namespace -f values.yml