#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

MASTER_HOST="$MASTER_NODE_IP $MASTER_NODE_HOSTNAME"
WORKER_HOST_1="$WORKER_NODE_1_IP $WORKER_NODE_1_HOSTNAME"
WORKER_HOST_2="$WORKER_NODE_2_IP $WORKER_NODE_2_HOSTNAME"
REGISTRY_HOST="$REGISTRY_NODE_IP $REGISTRY_NODE_DOMAIN"

grep -q "$MASTER_HOST" /etc/hosts || echo "$MASTER_HOST" | sudo tee -a /etc/hosts > /dev/null
grep -q "$WORKER_HOST_1" /etc/hosts || echo "$WORKER_HOST_1" | sudo tee -a /etc/hosts > /dev/null
grep -q "$WORKER_HOST_2" /etc/hosts || echo "$WORKER_HOST_2" | sudo tee -a /etc/hosts > /dev/null
grep -q "$REGISTRY_HOST" /etc/hosts || echo "$REGISTRY_HOST" | sudo tee -a /etc/hosts > /dev/null

cat /etc/hosts