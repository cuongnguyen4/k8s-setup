#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

cp -r $BASE/k8s/troubleshoot/containerd /etc

ls /etc/containerd

systemctl restart containerd

echo "OK"