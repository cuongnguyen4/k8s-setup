#!/bin/bash

sudo tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF

mkdir -p /etc/containerd
sudo containerd config default > /etc/containerd/config.toml

sudo systemctl restart containerd
sudo systemctl enable containerd
systemctl status containerd