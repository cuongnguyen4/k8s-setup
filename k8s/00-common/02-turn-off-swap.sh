#!/bin/bash

echo "Turning off swap"

sudo swapoff -a

sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# if swap couldn't removed completely, vi /etc/fstab then comment all lines conataining 'swap'