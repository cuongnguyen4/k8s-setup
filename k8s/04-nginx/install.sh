
#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh


cd $BASE/k8s/04-nginx

helm upgrade --install ingress-nginx ingress-nginx --namespace ingress-nginx --create-namespace -f values.yml

# helm template ingress-nginx ingress-nginx --namespace ingress-nginx --create-namespace -f values.yml
