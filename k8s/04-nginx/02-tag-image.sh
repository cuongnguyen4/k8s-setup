#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

docker tag ee54966f3891 ${LOCAL_REGISTRY}/ingress-nginx/controller:v1.10.1
docker tag 684c5ea3b61b ${LOCAL_REGISTRY}/ingress-nginx/kube-webhook-certgen:v1.4.1