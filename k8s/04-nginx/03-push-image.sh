#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

docker push ${LOCAL_REGISTRY}/ingress-nginx/controller:v1.10.1
docker push ${LOCAL_REGISTRY}/ingress-nginx/kube-webhook-certgen:v1.4.1
