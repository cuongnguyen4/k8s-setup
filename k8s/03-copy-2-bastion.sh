#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

ssh $BASTION_NODE_USER@$BASTION_NODE_IP 'rm -rf ~/k8s-setup && mkdir -p ~/k8s-setup'

scp -r $BASE/bastion $BASTION_NODE_USER@$BASTION_NODE_IP:~/k8s-setup/bastion
