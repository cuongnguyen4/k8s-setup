#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

ssh $WORKER_NODE_USER@$WORKER_NODE_1_IP 'rm -rf ~/k8s-setup && mkdir -p ~/k8s-setup'

scp -r $BASE/k8s $WORKER_NODE_USER@$WORKER_NODE_1_IP:~/k8s-setup/k8s
scp -r $BASE/bastion $WORKER_NODE_USER@$WORKER_NODE_1_IP:~/k8s-setup/bastion
