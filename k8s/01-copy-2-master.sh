#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

echo $MASTER_NODE_USER
echo $MASTER_NODE_IP

ssh $MASTER_NODE_USER@$MASTER_NODE_IP 'rm -rf ~/k8s-setup && mkdir -p ~/k8s-setup'


scp -r $BASE/k8s $MASTER_NODE_USER@$MASTER_NODE_IP:~/k8s-setup/k8s
scp -r $BASE/bastion $MASTER_NODE_USER@$MASTER_NODE_IP:~/k8s-setup/bastion
