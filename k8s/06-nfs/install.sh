#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

cd $BASE/k8s/06-nfs

helm upgrade --install nfs-provisioner nfs-server-provisioner -f values.yaml