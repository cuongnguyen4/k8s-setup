#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

cd $BASE/k8s/06-nfs/image

ls

for x in *.tar; do docker load < $x && echo "Loaded docker $x"; done;

docker tag registry.k8s.io/sig-storage/nfs-provisioner:v4.0.8 ${LOCAL_REGISTRY}/sig-storage/nfs-provisioner:v4.0.8

docker push ${LOCAL_REGISTRY}/sig-storage/nfs-provisioner:v4.0.8