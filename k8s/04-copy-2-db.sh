#!/bin/bash

# rsync -r -v ./scripts gtel-public-k8s-db:/root/k8s-setup
# rsync -r -v ./oracle gtel-public-k8s-db:/root/k8s-setup
# rsync -avR ./k8s/06-nfs gtel-public-k8s-db:/root/k8s-setup
# rsync -r -v ./bastion/docker-ce gtel-public-k8s-db:/root/k8s-setup/bastion
# rsync -r -v ./bastion/docker-compose gtel-public-k8s-db:/root/k8s-setup/bastion
rsync -r -v ./k8s/00-common gtel-public-k8s-db:/root/k8s-setup/k8s