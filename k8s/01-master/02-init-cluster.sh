#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

sudo kubeadm init \
  --pod-network-cidr=10.244.0.0/16 \
  --cri-socket unix:///run/containerd/containerd.sock \
  --apiserver-advertise-address=$MASTER_NODE_IP