#!/bin/bash

kubectl create secret generic rarep-keycloak-key --from-file=keycloak-public-key=jwt_keycloak.pub -n rarep
