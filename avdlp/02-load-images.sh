#!/bin/bash

source $(pwd)/scripts/00-setup-env.sh

cd $BASE/avdlp/image

ls

for x in *.tar; do docker load < $x && echo "Loaded docker $x"; done;

docker tag reg.aiontech.vn/rar/rarep-be:prod-4.0.2 ${LOCAL_REGISTRY}/rar/rarep-be:prod-4.0.2
docker tag reg.aiontech.vn/rar/rarep-fe-admin:prod-4.0.1 ${LOCAL_REGISTRY}/rar/rarep-fe-admin:prod-4.0.1
docker tag reg.aiontech.vn/rar/rarep-ts:prod-4.0.0 ${LOCAL_REGISTRY}/rar/rarep-ts:prod-4.0.0
docker tag reg.aiontech.vn/rar/rarep-tw:prod-4.0.0 ${LOCAL_REGISTRY}/rar/rarep-tw:prod-4.0.0
docker tag reg.aiontech.vn/aiontech/stm-be-auth:dev-24590 ${LOCAL_REGISTRY}/rar/rarep-be-auth:dev-24590
docker tag reg.aiontech.vn/aiontech/aionstore-be-syncer:master-24591 ${LOCAL_REGISTRY}/rar/rarep-be-syncer:master-24591

docker push ${LOCAL_REGISTRY}/rar/rarep-be:prod-4.0.2
docker push ${LOCAL_REGISTRY}/rar/rarep-fe-admin:prod-4.0.1
docker push ${LOCAL_REGISTRY}/rar/rarep-ts:prod-4.0.0
docker push ${LOCAL_REGISTRY}/rar/rarep-tw:prod-4.0.0
docker push ${LOCAL_REGISTRY}/rar/rarep-be-auth:dev-24590
docker push ${LOCAL_REGISTRY}/rar/rarep-be-syncer:master-24591